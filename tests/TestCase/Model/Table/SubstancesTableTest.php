<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SubstancesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SubstancesTable Test Case
 */
class SubstancesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SubstancesTable
     */
    public $Substances;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.substances'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Substances') ? [] : ['className' => 'App\Model\Table\SubstancesTable'];
        $this->Substances = TableRegistry::get('Substances', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Substances);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
