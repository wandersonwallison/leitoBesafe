<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Substances Model
 *
 * @method \App\Model\Entity\Substance get($primaryKey, $options = [])
 * @method \App\Model\Entity\Substance newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Substance[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Substance|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Substance patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Substance[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Substance findOrCreate($search, callable $callback = null, $options = [])
 */
class SubstancesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('substances');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('principio_ativo', 'create')
            ->notEmpty('principio_ativo');

        $validator
            ->requirePresence('cnpj', 'create')
            ->notEmpty('cnpj');

        $validator
            ->requirePresence('laboratorio', 'create')
            ->notEmpty('laboratorio');

        $validator
            ->numeric('registro')
            ->requirePresence('registro', 'create')
            ->notEmpty('registro');

        $validator
            ->numeric('ean')
            ->requirePresence('ean', 'create')
            ->notEmpty('ean');

        $validator
            ->requirePresence('produto', 'create')
            ->notEmpty('produto');

        $validator
            ->requirePresence('apresentacao', 'create')
            ->notEmpty('apresentacao');

        $validator
            ->requirePresence('classe_terapeutica', 'create')
            ->notEmpty('classe_terapeutica');

        return $validator;
    }
}
