<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Substance Entity
 *
 * @property int $id
 * @property string $principio_ativo
 * @property string $cnpj
 * @property string $laboratorio
 * @property float $registro
 * @property float $ean
 * @property string $produto
 * @property string $apresentacao
 * @property string $classe_terapeutica
 */
class Substance extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
