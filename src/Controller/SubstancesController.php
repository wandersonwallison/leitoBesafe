<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Substances Controller
 *
 * @property \App\Model\Table\SubstancesTable $Substances
 */
class SubstancesController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $substances = $this->paginate($this->Substances);

        $this->set(compact('substances'));
        $this->set('_serialize', ['substances']);
    }

    /**
     * View method
     *
     * @param string|null $id Substance id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $substance = $this->Substances->get($id, [
            'contain' => []
        ]);

        $this->set('substance', $substance);
        $this->set('_serialize', ['substance']);
    }

    public function addVarios() {
        //$jsonurl = "d://teste.json";
        $jsonurl = "c://listacmed.json";
        $json = file_get_contents($jsonurl);
        //$json = file_get_contents($jsonurl, 0, null, null);
        $json_output = json_decode($json);
        //debug(count($json_output->trends));
        //die();
        foreach ($json_output->trends as $trend) {
            $substance = $this->Substances->newEntity();
            $data = [
                'principio_ativo' => $trend->principio_ativo,
                'cnpj' => $trend->cnpj,
                'laboratorio' => $trend->laboratorio,
                'registro' => $trend->registro,
                'ean' => $trend->ean,
                'produto' => $trend->produto,
                'apresentacao' => $trend->apresentacao,
                'classe_terapeutica' => $trend->classe_terapeutica
            ];
            $substances = $this->Substances->patchEntity($substance, $data);
            $this->Substances->save($substances);
            $dados = "Adicionando substancias no banco";
            $this->set([
                'dados' => $dados,
                '_serialize' => ['dados']
            ]);
        }
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $substance = $this->Substances->newEntity();
        if ($this->request->is('post')) {
            $substance = $this->Substances->patchEntity($substance, $this->request->getData());
            if ($this->Substances->save($substance)) {
                $this->Flash->success(__('The substance has been saved.'));
            }
            $this->Flash->error(__('The substance could not be saved. Please, try again.'));
        }
        $this->set(compact('substance'));
        $this->set('_serialize', ['substance']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Substance id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $substance = $this->Substances->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $substance = $this->Substances->patchEntity($substance, $this->request->getData());
            if ($this->Substances->save($substance)) {
                $this->Flash->success(__('The substance has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The substance could not be saved. Please, try again.'));
        }
        $this->set(compact('substance'));
        $this->set('_serialize', ['substance']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Substance id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $substance = $this->Substances->get($id);
        if ($this->Substances->delete($substance)) {
            $this->Flash->success(__('The substance has been deleted.'));
        } else {
            $this->Flash->error(__('The substance could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

}
