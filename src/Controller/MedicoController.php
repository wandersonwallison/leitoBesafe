<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Medico Controller
 *
 * @property \App\Model\Table\MedicoTable $Medico
 */
class MedicoController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $medico = $this->paginate($this->Medico);

        $this->set(compact('medico'));
        $this->set('_serialize', ['medico']);
    }

    /**
     * View method
     *
     * @param string|null $id Medico id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $medico = $this->Medico->get($id, [
            'contain' => []
        ]);

        $this->set('medico', $medico);
        $this->set('_serialize', ['medico']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $medico = $this->Medico->newEntity();
        if ($this->request->is('post')) {
            $medico = $this->Medico->patchEntity($medico, $this->request->getData());
            if ($this->Medico->save($medico)) {
                $this->Flash->success(__('The medico has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The medico could not be saved. Please, try again.'));
        }
        $this->set(compact('medico'));
        $this->set('_serialize', ['medico']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Medico id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $medico = $this->Medico->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $medico = $this->Medico->patchEntity($medico, $this->request->getData());
            if ($this->Medico->save($medico)) {
                $this->Flash->success(__('The medico has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The medico could not be saved. Please, try again.'));
        }
        $this->set(compact('medico'));
        $this->set('_serialize', ['medico']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Medico id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $medico = $this->Medico->get($id);
        if ($this->Medico->delete($medico)) {
            $this->Flash->success(__('The medico has been deleted.'));
        } else {
            $this->Flash->error(__('The medico could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
